package io.librehealth.mhbs.neoroo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import io.librehealth.mhbs.neoroo.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}